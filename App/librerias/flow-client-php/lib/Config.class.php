<?php
/**
 * Clase para Configurar el cliente
 * @Filename: Config.class.php
 * @version: 2.0
 * @Author: flow.cl
 * @Email: csepulveda@tuxpan.com
 * @Date: 28-04-2017 11:32
 * @Last Modified by: Carlos Sepulveda
 * @Last Modified time: 28-04-2017 11:32
 */
 
 class Config {
 	
	function getNormal($name) {
        $COMMERCE_CONFIG = array(
            "APIKEY" => "25F2D6DB-34B6-4E85-97AC-765321CL169D", // Registre aquí su apiKey
            "SECRETKEY" => "dbf0673b06cc19a6a118a35ec5dc7d04a6cdd838", // Registre aquí su secretKey
            "APIURL" => "https://sandbox.flow.cl/api", // Producción EndPoint o Sandbox EndPoint
            "BASEURL" => "https://comprapindev.tk/flow", //Registre aquí la URL base en su página donde instalará el cliente
            "BASEURLVUE" => "https://comprapindev.tk/#/"
        );
        
		if(!isset($COMMERCE_CONFIG[$name])) {
			throw new Exception("The configuration element thas not exist" . $name, 1);
		}
		return $COMMERCE_CONFIG[$name];
	}

     static function get($name) {
         $COMMERCE_CONFIG = array(
             "APIKEY" => "25F2D6DB-34B6-4E85-97AC-765321CL169D", // Registre aquí su apiKey
             "SECRETKEY" => "dbf0673b06cc19a6a118a35ec5dc7d04a6cdd838", // Registre aquí su secretKey
             "APIURL" => "https://sandbox.flow.cl/api", // Producción EndPoint o Sandbox EndPoint
             "BASEURL" => "https://comprapindev.tk/flow", //Registre aquí la URL base en su página donde instalará el cliente
             "BASEURLVUE" => "https://comprapindev.tk/#/"
         );

         if(!isset($COMMERCE_CONFIG[$name])) {
             throw new Exception("The configuration element thas not exist" . $name, 1);
         }
         return $COMMERCE_CONFIG[$name];
     }
 }
